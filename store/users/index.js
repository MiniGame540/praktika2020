export const state = () => ({
  data: [],
  success: false,
  error: {
    status: false,
    info: null
  },
  meta: {
    page: 1,
    isLoading: true,
    isLoaded: false,
    isMore: true,
    loadWidth: null
  }
})
