export default {
  setData (state, items) {
    state.data = items
  },
  addData (state, items) {
    state.data = [...state.data, ...items]
  },
  setUsersSuccess (state, status) {
    state.success = status
  },
  setUsersError (state, payload) {
    state.error.status = payload.status
    state.error.info = payload.info
  },
  setUsersMeta (state, payload) {
    state.meta = {
      ...state.meta,
      ...payload
    }
  }
}
