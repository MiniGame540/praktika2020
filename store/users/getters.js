export default {
  getData (state) {
    return state.data
  },
  getMeta (state) {
    return state.meta
  },
  getResponse (state) {
    return {
      success: state.success,
      error: { ...state.error }
    }
  }
}
