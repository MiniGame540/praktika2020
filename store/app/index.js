export const state = () => ({
  mobileMenu: false,
  locales: ['en', 'de'],
  locale: 'en'
})
