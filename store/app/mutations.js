export default {
  toggle (state) {
    if (!state.mobileMenu) {
      // scrollLock.enable()
    } else {
      // scrollLock.disable()
    }
    state.mobileMenu = !state.mobileMenu
  },
  setLang (state, locale) {
    if (state.locales.includes(locale)) {
      state.locale = locale
    }
  }
}
