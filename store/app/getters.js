export default {
  mobileMenu (state) {
    return state.mobileMenu
  },
  lang (state) {
    return state.locale
  }
}
