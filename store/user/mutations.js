export default {
  setUser (state, user) {
    state.user = user
  },
  setUserError (state, mark) {
    state.error = mark
  },
  setUserSuccess (state, mark) {
    state.success = mark
  }
}
