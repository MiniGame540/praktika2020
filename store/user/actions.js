import { getUserAPI } from '~/api/user'

export default {
  async fetchUser ({ commit }) {
    const data = await getUserAPI()
    if (data.status !== 200) {
      commit('setUserError', true)
      commit('setUserSuccess', false)
      return
    }
    commit('setUser', data.result)
    commit('setUserError', false)
    commit('setUserSuccess', true)
  }
}
