export default {
  getUser (state) {
    return state.user
  },
  getMeta (state) {
    return {
      success: state.success,
      error: state.error
    }
  }
}
