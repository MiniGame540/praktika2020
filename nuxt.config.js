export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/scss/global.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  router: {
    middleware: 'i18n'
  },
  generate: {
    routes: ['/', '/de']
  },
  plugins: [
    {
      src: '@/plugins/vClickOutside',
      ssr: false
    },
    '~/plugins/i18n.js'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify',
    '@nuxtjs/gtm'
  ],
  gtm: {
    id: 'GTM-MTPPQWZ',
    enabled: true
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    'nuxt-webfontloader',
    '@nuxtjs/svg',
    ['@nuxtjs/sitemap', {
      hostname: 'https://nuxt-task-4s1s9adxh.vercel.app',
      i18n: true,
      trailingSlash: false,
      defaults: {
        changefreq: 'monthly',
        priority: 1,
        lastmod: new Date()
      }
    }]
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {},
  webfontloader: {
    google: {
      families: ['Source+Sans+Pro:400,700', 'Overpass:400,600']
    }
  },
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    defaultAssets: false,
    treeShake: true,
    theme: {
      themes: {}
    }
  },
  /*
  ** Environment variables for production
  */
  env: {
    baseUrl: process.env.BASE_URL || 'https://frontend-test-assignment-api.abz.agency/api/v1'
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    templates: [
      {
        src: './templates/robots.txt',
        dst: '../static/robots.txt',
        options: {
          env: process.env.NODE_ENV
        }
      }
    ],
    extend (config, ctx) {
    }
  }
}
