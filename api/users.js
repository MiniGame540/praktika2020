import { instance } from './instance'

export const getUsersAPI = async (page = 1, count = 6) => {
  try {
    const result = await instance.get(`/users?page=${page}&count=${count}`)
    return {
      result: result.data.users,
      links: result.data.links,
      status: result.status
    }
  } catch ({ response }) {
    return {
      status: response.status,
      result: response.data.message
    }
  }
}
