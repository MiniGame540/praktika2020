import { instance } from './instance'

export const getUserAPI = async () => {
  try {
    const result = await instance.get('/users/1')
    return {
      result: result.data.user,
      status: result.status
    }
  } catch ({ response }) {
    return {
      status: response.status
    }
  }
}
